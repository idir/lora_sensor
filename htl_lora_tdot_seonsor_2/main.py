from network import LoRa
import socket
import time
import binascii
import machine
from machine import Pin

import struct

# pysense libraries
from pysense import Pysense
from LIS2HH12 import LIS2HH12
from SI7006A20 import SI7006A20
from LTR329ALS01 import LTR329ALS01
from MPL3115A2 import MPL3115A2,ALTITUDE,PRESSURE

# pysense objekte
py = Pysense()
mp = MPL3115A2(py,mode=ALTITUDE) # Returns height in meters. Mode may also be set to PRESSURE, returning a value in Pascals
si = SI7006A20(py)
lt = LTR329ALS01(py)
li = LIS2HH12(py)


# Initialize LoRa in LORAWAN mode.
lora = LoRa(mode=LoRa.LORAWAN)
# create an OTAA authentication parameters
app_eui = binascii.unhexlify('70B3D57ED000811B')
app_key = binascii.unhexlify('F42CE0D1C2C74A662CC5441CEB46A557')
# join a network using OTAA (Over the Air Activation)
lora.join(activation=LoRa.OTAA, auth=(app_eui, app_key), timeout=0)
# wait until the module has joined the network
while not lora.has_joined():
    time.sleep(2.5)
    print('Not joined yet ...')
# create a LoRa socket
s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)
# set the LoRaWAN data rate
s.setsockopt(socket.SOL_LORA, socket.SO_DR, 5)
# Things to do every 30 seconds
while True:
    s.setblocking(True)
    # send distance
    # s.send(b'' + 't' + str(si.temperature()) + 'l' + str(lt.light()) + 'h' + str(si.humidity()) + 'alt' + str(mp.altitude()) + 'r' + str(li.roll()) + 'a' + str(li.acceleration())+ 'p' + str(li.pitch()) + 'y' + str(li.yaw()))
    #s.send(b'' + 'hansi')
    s.send(b'' + 't' + str(mp.temperature()) + 'l' + str(lt.light()) + 'h' + str(si.humidity()) + 'a' + str(mp.altitude()))
    # t temp, l light, h humidity, p pressure, r roll,a acceleration, r rol, p pitch, y yaw
    s.setblocking(False)
    time.sleep(60)

    #print(py.read_battery_voltage())
    #print(mp.temperature())
    #print(mp.altitude())
    #mpp = MPL3115A2(py,mode=PRESSURE) # Returns pressure in Pa. Mode may also be set to ALTITUDE, returning a value in meters
    #print(mpp.pressure())
    #print(si.temperature())
    #print(si.humidity())
    #print(lt.light())
    #print(li.acceleration())
    #print(li.roll())
    #print(li.pitch())
    #print(li.yaw())